//
//  ViewController.swift
//  Home task 2
//
//  Created by Александр Зарудний on 9/15/21.
//

import UIKit
import SnapKit

class ViewController: UITableViewController {
    
    //MARK: ~identifier
    let cellID = "cellID"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = 80.0
        tableView.register(UITableViewCellCustom.self, forCellReuseIdentifier: cellID)
    }
    
    //MARK: ~dataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1000
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! UITableViewCellCustom
        cell.cellTitle.text = cell.entityObject.title + " \(indexPath.row + 1)"
        cell.cellDescription.text = cell.entityObject.description + " \(indexPath.row + 1)"
        return cell
    }
    
    //MARK: ~delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? UITableViewCellCustom
        let secondController = SecondVC()
        
        secondController.imageView.image = cell?.cellImage.image
        secondController.titleLabel.text = cell?.cellTitle.text
        secondController.descriptionLabel.text = cell?.cellDescription.text
        
        let navigationController = UINavigationController(rootViewController: secondController)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
}


