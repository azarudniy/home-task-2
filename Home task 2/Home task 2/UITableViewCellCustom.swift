//
//  UITableViewCellCustom.swift
//  Home task 2
//
//  Created by Александр Зарудний on 9/18/21.
//

import UIKit
import SnapKit

class UITableViewCellCustom: UITableViewCell {
    
    //MARK: ~properties
    let entityObject = Entity(image: UIImage(named: "baseline_power_settings_new_white_24pt.png") ?? UIImage(), title: "Title", description: "Description")
    var cellImage = UIImageView()
    var cellTitle = UILabel()
    var cellDescription = UILabel()
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setViewsAtCell()
    }
    
    private func setViewsAtCell() {
        cellImage.image = entityObject.image
        cellTitle.text = ""
        cellDescription.text = ""
        customizationImage()
        customizationTitle()
        customizationDescription()
    }
    
    
    //MARK: ~castomization views
    private func customizationImage() {
        addSubview(cellImage)
        cellImage.backgroundColor = .lightGray
        cellImage.snp.makeConstraints {
            $0.width.equalTo(40)
            $0.leading.equalToSuperview().inset(20)
            $0.top.equalToSuperview().inset(20)
            $0.bottom.equalToSuperview().inset(20)
        }
        cellImage.layer.cornerRadius = 20
    }
    
    private func customizationTitle() {
        addSubview(cellTitle)
        cellTitle.snp.makeConstraints {
            $0.leading.equalTo(cellImage.snp.trailing).offset(20)
            $0.top.equalToSuperview().inset(20)
        }
    }
    
    private func customizationDescription() {
        addSubview(cellDescription)
        cellDescription.font = UIFont(name: "HelveticaNeue", size: 16.0)
        cellDescription.textColor = .darkGray
        cellDescription.snp.makeConstraints {
            $0.top.equalTo(cellTitle.snp.bottom).offset(5)
            $0.leading.equalTo(cellTitle)
        }
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
