//
//  SecondVC.swift
//  Home task 2
//
//  Created by Александр Зарудний on 9/19/21.
//

import UIKit

class SecondVC: UIViewController {
    
    //MARK: ~views
    lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = .lightGray
        image.layer.cornerRadius = 40
        return image
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    //MARK: ~backBarButton
    lazy var backBarButton: UIBarButtonItem = {
        UIBarButtonItem.init(title: "Back", style: .plain, target: self, action: #selector(backClicked))
    }()
    
    @objc func backClicked(sender: UIBarButtonItem!) {
        self.dismiss(animated: true, completion: nil)
    }
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setViews()
        navigationItem.leftBarButtonItem = backBarButton
        
    }
    
    
    //MARK: ~set views on ViewController
    private func setViews() {
        view.addSubview(imageView)
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        
        imageView.snp.makeConstraints {
            $0.width.height.equalTo(80)
            $0.center.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(imageView.snp.bottom).offset(20)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(titleLabel.snp.bottom).offset(20)
        }
    }
}
