//
//  Entity.swift
//  Home task 2
//
//  Created by Александр Зарудний on 9/18/21.
//

import UIKit

//MARK: ~Entity
struct Entity {
    var image: UIImage
    var title: String
    var description: String
    
}
